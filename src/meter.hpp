#ifndef METER_H
#define METER_H

class Meter
{
private:
    int pin;
    void (*countISR)();

public:
    Meter(int pin, void (*countISR)())
    {
        this->pin = pin;
        this->countISR = countISR;
        wakeup();
    }

    void sleep()
    {
        detachInterrupt(pin);
        pinMode(pin, INPUT); //no pull down
    }

    void wakeup()
    {
        pinMode(pin, INPUT_PULLDOWN);
        attachInterrupt(pin, this->countISR, RISING); //assume we have hw debounce
    }
};

#endif