#ifndef LED_H
#define LED_H

class Led
{
private:
    int pin;

public:
    Led(int pin)
    {
        this->pin = pin;
    }
    void on() //TODO enable low or high
    {
        digitalWrite(pin, HIGH);
    }
    void off() //TODO enable low or high
    {
        digitalWrite(pin, LOW);
    }
    bool isOn()
    {
        if (HIGH == digitalRead(pin))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    void sleep()
    {
        pinMode(pin, INPUT); //High impedance, not sure it is supported
    }
    void wakeup()
    {
        pinMode(pin, OUTPUT); //set pin pin as output
    }
};

#endif