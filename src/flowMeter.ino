#include "Particle.h"
#include "led.hpp"
#include "meter.hpp"

const int COUNTER_STEP = 5;                   //liters
const int TICK_COUNT = 1000;                  //mS
const int LED_ON_TIME = (int)(1000 * 60);     //1 min
const int SLEEP_TIME = (int)(1000 * 60 * 20); //20 mins

const char *sleepEvent = "going-to-sleep";
const char *wakeupEvent = "waking-up";
const char *countOverflowEvent = "counter-overflow";

SYSTEM_MODE(SEMI_AUTOMATIC);

Led led(D0);
Meter meter(D1, countISR);
Timer ledOnTimer(LED_ON_TIME, ledOnTimerHandler);

volatile int countInLiters = 0;
// Had to use this extra variable to publish since adding a volatile variable throws an error, dnt know why yet
int count = 0; //non volatile count variable

void setup()
{
    Particle.variable("litersCount", &count, INT); //register variable to be called when needed

    connect();
}

void loop()
{
    count = countInLiters; //update non volatile count variable
    if (0 == (countInLiters % COUNTER_STEP))
    {
        if (!led.isOn()) //Turn on led if off
        {
            led.on();
        }
        if (!ledOnTimer.isActive()) //Start on timer if not started
        {
            ledOnTimer.start();
        }

        Particle.publish(countOverflowEvent, PRIVATE);
    }
}

void countISR()
{
    countInLiters++; //need to handle int overflow?
}

void ledOnTimerHandler()
{
    ledOnTimer.stop();
    led.off();
    sleep();
}

void sleep()
{
    Particle.publish(sleepEvent, PRIVATE);

    //sleep peripherals
    meter.sleep();
    led.sleep();

    Particle.disconnect(); //disconnect from cloud
    countInLiters = 0; //reset count

    //sleep for SLEEP_TIME duration
    SystemSleepConfiguration config;
    config.mode(SystemSleepMode::HIBERNATE)
        .duration(SLEEP_TIME);
    System.sleep(config);

    //resumes here after sleep
    connect();
    Particle.publish(wakeupEvent, PRIVATE);

    //wake peripherals
    led.wakeup();
    meter.wakeup();
}

void connect()
{
    //What to do fo failing connnections?
    if (!Particle.connected())
    {
        Particle.connect();
    }
}
